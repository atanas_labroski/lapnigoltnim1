package mk.atanas.lapnigoltnim1.app;

import android.content.Context;
import mk.atanas.lapnigoltnim1.NoInternetConnectionException;
import mk.atanas.lapnigoltnim1.model.LokalsRepository;
import mk.atanas.lapnigoltnim1.model.MunicipalitiesRepository;
import mk.atanas.lapnigoltnim1.utils.ConnectionDetector;

/**
 * Created by Atanas on 5/10/14.
 */
public class DataController {
    private Context context;
    private ConnectionDetector connectionDetector;
    public MunicipalitiesRepository municipalitiesRepository;
    public LokalsRepository lokalsRepository;

    private static DataController ourInstance = new DataController();

    public static DataController getInstance() {
        return ourInstance;
    }

    /**
     * The controller is set to private so we cannot instantiate this class
     */
    private DataController() {

    }

    public static void prepareDataController(Context context) {
        getInstance().context = context;
    }

    public boolean isConnected() {
        if(connectionDetector == null) {
            connectionDetector = new ConnectionDetector(context);
        }
        return connectionDetector.isConnectingToInternet();
    }

    public void fetchData() throws NoInternetConnectionException {
        if(!isConnected()) {
            throw new NoInternetConnectionException();
        }

        municipalitiesRepository = new MunicipalitiesRepository();
        lokalsRepository = new LokalsRepository();
        FetchData.fetchMunicipalities(municipalitiesRepository);
        FetchData.fetchLokals(lokalsRepository);
    }

}
