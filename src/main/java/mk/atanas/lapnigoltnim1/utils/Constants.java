package mk.atanas.lapnigoltnim1.utils;

/**
 * Created by Atanas on 5/10/14.
 */
public class Constants {

    public static final String URL_GET_LOKALI = "http://lapnigoltni.mk/androidv2/lokali_json.php";

    public static final String URL_GET_LOKALI_TAG = "lapni_goltni_get_lokali";

    public static final String URL_LOGO = "http://lapnigoltni.mk/admin/";

    public static final String URL_GET_MUNICIPALITIES = "http://lapnigoltni.mk/androidv2/opstini_json.php";

    public static final String URL_GET_MUNICIPALITIES_TAG = "lapni_goltni_get_opstini";

    public static final int MAX_LOADS = 2;

}
