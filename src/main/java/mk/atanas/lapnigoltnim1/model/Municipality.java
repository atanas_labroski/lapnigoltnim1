package mk.atanas.lapnigoltnim1.model;

/**
 * Created by Atanas on 5/10/14.
 */
public class Municipality {
    private int ID;
    private String name;

    public Municipality(int ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
