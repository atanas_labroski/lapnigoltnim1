package mk.atanas.lapnigoltnim1.model;

import android.util.Log;
import mk.atanas.lapnigoltnim1.app.DataController;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Atanas on 5/10/14.
 */
public class LokalsRepository {
    private static final String TAG = LokalsRepository.class.getSimpleName();

    private List<Lokal> lokals;

    private static final String LokalID = "lid";
    private static final String LokalName = "ime";
    private static final String LokalLink = "link";
    private static final String LokalDescription = "opis";
    private static final String LokalCity = "grad";
    private static final String LokalType = "tip";
    private static final String LokalLogo = "logo";
    private static final String LokalWorkingTime = "rabvreme";
    private static final String LokalWTDay = "den";
    private static final String LokalWTHours = "cas";
    private static final String LokalMunicipalities = "opstini";
    private static final String LokalMunicipality = "opstina";
    private static final String LokalLocations = "lokacii";

    private static final String LokalLocation = "lokacii";
    private static final String LocationDeliveryType = "tipdostava";
    private static final String LocationMunicipality = "opstina";
    private static final String TAG_VIP = "vip";
    private static final String TAG_TMOBILE = "tmobile";
    private static final String TAG_ONE = "one";
    private static final String TAG_FIKSEN = "fiksen";
    private static final String LocationAddress = "address";

    public LokalsRepository() {
        lokals = new ArrayList<Lokal>();
    }

    public void clear() {
        lokals.clear();
    }

    public void addLokal(Lokal lokal) {
        lokals.add(lokal);
    }

    public Lokal findById(int id) {
        if (lokals.size() != 0) {
            for (Lokal lokal : lokals) {
                if (lokal.getID() == id) {
                    return lokal;
                }
            }
        }
        return null;
    }

    public void createFromJSONArray(JSONArray jsonArray) {
        if (jsonArray.length() == 0) {
            return;
        }

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Lokal lokal = new Lokal(
                        jsonObject.getInt(LokalID),
                        jsonObject.getString(LokalName),
                        jsonObject.getString(LokalLink),
                        jsonObject.getString(LokalDescription),
                        jsonObject.getString(LokalCity),
                        jsonObject.getString(LokalType),
                        jsonObject.getString(LokalLogo)
                );
                lokal.setWorkTime(createDaysAndTimes(jsonObject.getJSONArray(LokalWorkingTime)));
                lokal.setMunicipalities(createMunicipalities(jsonObject.getJSONArray(LokalMunicipalities)));
                lokal.setLocations(createLocations(jsonObject.getJSONArray(LokalLocations)));
                if (findById(lokal.getID()) == null) {
                    lokals.add(lokal);
                }
                Log.i(TAG, "lokal added to repository:" + lokal.toString() + ", locals size:" + lokals.size());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public ArrayList<DaysAndTime> createDaysAndTimes(JSONArray jsonArray) {
        ArrayList<DaysAndTime> daysAndTimes = new ArrayList<DaysAndTime>();
        if (jsonArray.length() != 0) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    DaysAndTime daysAndTime = new DaysAndTime(jsonObject.getString(LokalWTDay),
                            jsonObject.getString(LokalWTHours));
                    daysAndTimes.add(daysAndTime);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
//        Log.i(TAG, "createDaysAndTime: size-" + daysAndTimes.size());
        return daysAndTimes;
    }

    public ArrayList<Municipality> createMunicipalities(JSONArray jsonArray) {
        ArrayList<Municipality> municipalities = new ArrayList<Municipality>();

        if (jsonArray.length() != 0) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Municipality municipality = DataController.getInstance().municipalitiesRepository.findByName(
                            jsonObject.getString(LokalMunicipality)
                    );
                    municipalities.add(municipality);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

//        Log.i(TAG, "createMunicipalities: size-" + municipalities.size());
        return municipalities;
    }

    public ArrayList<Location> createLocations(JSONArray jsonArray) {
        ArrayList<Location> locations = new ArrayList<Location>();

        if (jsonArray.length() != 0) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Municipality municipality = DataController.getInstance().municipalitiesRepository.findByName(
                            jsonObject.getString(LokalMunicipality)
                    );
                    Location location = new Location(
                            jsonObject.getString(LocationDeliveryType),
                            municipality,
                            jsonObject.getString(LocationAddress)
                    );
                    location.addPhone(new Phone(jsonObject.getString(TAG_FIKSEN), TAG_FIKSEN));
                    location.addPhone(new Phone(jsonObject.getString(TAG_TMOBILE), TAG_TMOBILE));
                    location.addPhone(new Phone(jsonObject.getString(TAG_ONE), TAG_ONE));
                    location.addPhone(new Phone(jsonObject.getString(TAG_VIP), TAG_VIP));
                    locations.add(location);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }

//        Log.i(TAG, "createLocations: size-" + locations.size());
        return locations;
    }

    public List<Lokal> getLokals() {
        return lokals;
    }
    
}
