package mk.atanas.lapnigoltnim1.model;

/**
 * Created by Atanas on 5/10/14.
 */
public class Phone {
    private static final String TAG = Phone.class.getSimpleName();

    private String phone;
    private String operator;

    public Phone(String phone, String operator) {
        this.phone = phone;
        this.operator = operator;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
