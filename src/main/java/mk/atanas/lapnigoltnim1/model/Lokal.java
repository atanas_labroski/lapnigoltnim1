package mk.atanas.lapnigoltnim1.model;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.NetworkImageView;
import mk.atanas.lapnigoltnim1.R;
import mk.atanas.lapnigoltnim1.app.AppController;
import mk.atanas.lapnigoltnim1.utils.Constants;

import java.util.ArrayList;

/**
 * Created by Atanas on 5/10/14.
 */
public class Lokal {
    private static final String TAG = Lokal.class.getSimpleName();

    private int ID;
    private String name;
    private String link;
    private String description;
    private String city;
    private String type;
    private String logoUrl;
    private ArrayList<DaysAndTime> workTime;
    private ArrayList<Municipality> municipalities;
    private ArrayList<Location> locations;

    public Lokal(int ID, String name, String link, String description, String city, String type, String logoUrl) {
        this.ID = ID;
        this.name = name;
        this.link = link;
        this.description = description;
        this.city = city;
        this.type = type;
        this.logoUrl = logoUrl;
        this.workTime = new ArrayList<DaysAndTime>();
        this.municipalities = new ArrayList<Municipality>();
        this.locations = new ArrayList<Location>();
    }

    public static String getTag() {
        return TAG;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public ArrayList<DaysAndTime> getWorkTime() {
        return workTime;
    }

    public void setWorkTime(ArrayList<DaysAndTime> workTime) {
        this.workTime = workTime;
    }

    public ArrayList<Municipality> getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(ArrayList<Municipality> municipalities) {
        this.municipalities = municipalities;
    }

    public ArrayList<Location> getLocations() {
        return locations;
    }

    public void setLocations(ArrayList<Location> locations) {
        this.locations = locations;
    }

    @Override
    public String toString() {
        String string = "";
        string += "LokalID: " + this.getID() + "\n";
        string += "LokalName: " + this.getName() + "\n";
        return string;
    }

    public void loadListItemView(View view) {

        TextView lokalName = (TextView) view.findViewById(R.id.lli_name);
        TextView lokalId = (TextView) view.findViewById(R.id.lli_lid);
        TextView lokalCity = (TextView) view.findViewById(R.id.lli_city);
        TextView lokalMunicipality = (TextView) view.findViewById(R.id.lli_municipality);
        TextView lokalWorkTime = (TextView) view.findViewById(R.id.lli_workTime);

        NetworkImageView lokalLogo = (NetworkImageView) view.findViewById(R.id.lli_image_logo);
        ImageView lokalType = (ImageView) view.findViewById(R.id.lli_imageview_type);

        lokalName.setText(this.getName());
        lokalId.setText("" + this.getID());
        lokalCity.setText(this.getCity());
        //TODO al 6/2/2014 11:22 implement municipalities string and work time string
        lokalMunicipality.setText("Municipalities");
        lokalWorkTime.setText("WorkTimes");

        link = resolveLink(link);

        if(type.equals("Пицерија")) {
            lokalType.setImageResource(R.drawable.pizza32x32);
        }
        if(type.equals("Сендвичара")) {
            lokalType.setImageResource(R.drawable.hamburger32x32);
        }
        if(type.equals("Кујна")) {
            lokalType.setImageResource(R.drawable.kujna32x32);
        }

        lokalLogo.setImageUrl(Constants.URL_LOGO + (logoUrl.replaceAll(" ", "%20")), AppController.getInstance().getImageLoader());

        Log.i(TAG, "loaded View for lokal: " + this.getID() + " " + this.getName());
    }

    private String resolveLink(String link) {
        String linkReturn = link;
        if(linkReturn.equals("null")){
            Log.i(TAG, "link is null");
        } else {
            if(!linkReturn.startsWith("http://") && !linkReturn.startsWith("https://")) {
                linkReturn = "http://" + linkReturn;
            }
        }
        return linkReturn;
    }
}
