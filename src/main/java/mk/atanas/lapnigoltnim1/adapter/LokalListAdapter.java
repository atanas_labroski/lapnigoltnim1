package mk.atanas.lapnigoltnim1.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import mk.atanas.lapnigoltnim1.R;
import mk.atanas.lapnigoltnim1.app.DataController;
import mk.atanas.lapnigoltnim1.model.Lokal;
import org.w3c.dom.Text;
import roboguice.inject.InjectView;

import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Atanas on 6/2/2014.
 */
public class LokalListAdapter extends BaseAdapter {
    private static final String TAG = LokalListAdapter.class.getSimpleName();

    private List<Lokal> data;
    private Activity activity;
    private LayoutInflater layoutInflater;

    public LokalListAdapter(Activity activity) {
        data = DataController.getInstance().lokalsRepository.getLokals();
        this.activity = activity;
        this.layoutInflater = LayoutInflater.from(activity);
        for (Lokal lokal : data) {
            Log.i(TAG, "lokal: " + lokal.toString());
        }
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        if (position < data.size() - 1) {
            return data.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        if (position < data.size() - 1) {
            Lokal lokal = data.get(position);
            return lokal.getID();
        } else {
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (convertView == null) {
            view = layoutInflater.inflate(R.layout.lokal_li, null);
        }

        Lokal lokal = data.get(position);
        lokal.loadListItemView(view);

        return view;

    }
}
