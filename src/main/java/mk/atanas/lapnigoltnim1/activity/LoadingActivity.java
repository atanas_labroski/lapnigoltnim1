package mk.atanas.lapnigoltnim1.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import mk.atanas.lapnigoltnim1.NoInternetConnectionException;
import mk.atanas.lapnigoltnim1.R;
import mk.atanas.lapnigoltnim1.app.AppController;
import mk.atanas.lapnigoltnim1.app.DataController;
import mk.atanas.lapnigoltnim1.model.LoadingType;
import mk.atanas.lapnigoltnim1.utils.Constants;
import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;

import java.util.HashMap;
import java.util.Map;

import static mk.atanas.lapnigoltnim1.utils.Constants.MAX_LOADS;

/**
 * Created by Atanas on 6/1/2014.
 */
@ContentView(R.layout.loading_activity)
public class LoadingActivity extends RoboActivity {
    private static final String TAG = LoadingActivity.class.getSimpleName();

    private int loadingCount = 0;
    private Map<LoadingType, String> loadFlagsMap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadFlagsMap = new HashMap<LoadingType, String>();

        AppController.getInstance().setLoadingActivity(this);

        new LoadData().execute();


    }

    public void finishedLoading(LoadingType loadingType) {
        Log.i(TAG, "FinishLoading: " + loadingType);
        loadFlagsMap.put(loadingType, "loaded");
        loadingCount++;
        if(loadingCount == MAX_LOADS) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            this.finish();
            Log.i(TAG, "Loading Finished");
        }
    }

    class LoadData extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                DataController.getInstance().fetchData();
            } catch (NoInternetConnectionException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}