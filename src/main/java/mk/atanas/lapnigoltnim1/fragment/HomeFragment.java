package mk.atanas.lapnigoltnim1.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import mk.atanas.lapnigoltnim1.R;
import mk.atanas.lapnigoltnim1.adapter.LokalListAdapter;
import mk.atanas.lapnigoltnim1.app.DataController;
import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Atanas on 5/11/14.
 */
public class HomeFragment extends Fragment {
    private static final String TAG = HomeFragment.class.getSimpleName();

    private ListView lokalListView;
    private ProgressBar progressBar;
    private TextView textViewMessage;
    private LokalListAdapter lokalListAdapter;

    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        lokalListView = (ListView) rootView.findViewById(R.id.listView_lokals);
        progressBar = (ProgressBar) rootView.findViewById(R.id.pb_loading);
        textViewMessage = (TextView) rootView.findViewById(R.id.lokal_message);

        new LoadLokals(this.getActivity()).execute();

        lokalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.lli_lid);
                Log.i(TAG, "LokalListItemClicked: " + DataController.getInstance().lokalsRepository.findById(
                        Integer.parseInt(textView.getText().toString())).toString());
            }
        });
        return rootView;
    }

    class LoadLokals extends AsyncTask<String, String, String> {
        private Activity activity;

        LoadLokals(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            lokalListView.setVisibility(View.GONE);
            textViewMessage.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            lokalListAdapter = new LokalListAdapter(activity);
            Log.i(TAG, "loadingLokalListView");
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    lokalListView.setAdapter(lokalListAdapter);
                    lokalListView.setVisibility(View.VISIBLE);
                    textViewMessage.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }
}