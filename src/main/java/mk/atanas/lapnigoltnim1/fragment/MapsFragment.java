package mk.atanas.lapnigoltnim1.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import mk.atanas.lapnigoltnim1.R;

/**
 * Created by Atanas on 5/28/2014.
 */
public class MapsFragment extends Fragment {

    /**
     * Default constructor.
     */
    public MapsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_maps, container, false);
        return rootView;
    }
}
